# Scotland Batch Running

Python script which creates a PyQT interface from which a batch file can be generated and run.  The interface allows the user to select the TUFLOW exectuable and TCF and then select from the range of
different options and genertae a batch file or run the batch file.  The options are a series of scenarios, variables and events as described here:  
The scenarios, events and variables are hard-coded in but this example could be modified for another set of scenarios, variables and events.

## Dependencies

The script requires the installation of PyQT and its dependencies as well as the sys library.

## Quickstart

The Python script requires running of the app.py script.

1. Run the app.py script.  The interface should appear
2. Browse to and select the TUFLOW executable and TUFLOW Control file (TCF).
3. Select the relevent cell size.
4. Select the relevent Annual Exceedance Probability (AEP), storm duration, climate change horizon and scenario as well as any model domains to be simulated.  Multiple options can be selected.
5. To create the batch file, select the 'Create Batch File.  This will generate a batch file in the same location as the TCF selected in step 2.
6. To run the simulation, select the 'Run batch File' option, this will generate a batch file in the same location as the TCF selected in step 2 and run the batch file to set the TUFLOW simulations going.

## More Details

Please see the project wiki page for more explanation and pretty pictures.

## Changelog

0.1
* first commit